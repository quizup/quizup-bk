import { app } from './server';
import { ENV, mongooseConn, logger } from './config';

const start = async () => {
	// Pre requisites
	if (!ENV.JWT_SECRET) {
		throw new Error('JWT_SECRET must be defined');
	}

	if (!ENV.MONGO_URI) {
		throw new Error('MONGO_URI must be defined');
	}

	// Connect to mongoose
	await mongooseConn();

	// Start server
	app.listen(ENV.PORT, () => {
		logger.info(`Listening on port ${ENV.PORT}!!!`);
	});
};

// start server
start();
