import { Router } from 'express';

import * as UserController from './user.controller';
import { verifyToken } from './middlewares/auth.middleware';

const router = Router();

router.post('/login', UserController.login).get('/whoami', verifyToken, UserController.whoami).post('/register', UserController.register);

export const UserRoutes = router;
