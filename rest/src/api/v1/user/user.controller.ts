import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';

import { ApiResponse, _hashText, _compareHash } from '../../../shared';
import { IUser } from './user.interface';
import * as UserService from './user.service';
import { ENV } from '../../../../config';

export const login = async (req: Request, res: Response) => {
	try {
		const payload: IUser = req.body;
		const _user = await UserService.getUser(payload.email);

		// Check if user exists or not
		if (_user) {
			const _validUser = _compareHash(payload.password, _user.password);

			// Validate user password
			if (_validUser) {
				const token = jwt.sign(_user, ENV.JWT_SECRET, { expiresIn: ENV.JWT_EXPIRES_IN });

				const resUser = { name: _user.name, email: _user.email };

				res.send(new ApiResponse(true, `Welcome ${_user?.name}`, { token, user: resUser }));
			} else {
				res.send(new ApiResponse(true, `Invalid credentials`));
			}
		} else {
			res.send(new ApiResponse(true, `User not found`));
		}
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not login', undefined, error.message));
	}
};

export const whoami = async (req: Request, res: Response) => {
	try {
		res.send(new ApiResponse(true, 'Profile info fetched', req.body));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get your profile', undefined, error.message));
	}
};

export const register = async (req: Request, res: Response) => {
	try {
		const payload: IUser = req.body;
		const _user = await UserService.getUser(payload.email);

		// Check if user already exists
		if (_user) {
			res.send(new ApiResponse(true, `Please login to continue`));
		} else {
			const _newUser = await UserService.saveUser(payload);
			res.send(new ApiResponse(true, `Welcome ${_newUser?.name}`, _newUser));
		}
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get your profile', undefined, error.message));
	}
};
