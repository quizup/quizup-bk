import { Request, Response, NextFunction } from 'express';

import { ApiResponse, EUserRoles } from '../../../../shared';

export const verifyRole = (req: Request, res: Response, next: NextFunction) => {
	try {
		if (req.body.user.role === EUserRoles.admin) next();
		else res.send(new ApiResponse(false, 'Unauthorized access'));
	} catch (err) {
		res.send(new ApiResponse(false, err.message, err));
	}
};
