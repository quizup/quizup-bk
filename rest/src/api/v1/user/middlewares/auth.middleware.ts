import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

import { ApiResponse } from '../../../../shared';
import { ENV } from '../../../../../config';
import { IUserModel } from '../user.model';

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
	try {
		const authHeader = (req.headers.authorization || req.headers['token']) as string | undefined;

		if (!authHeader) throw new Error('No auth token provided');

		const token = authHeader.split(' ')[1];
		const decoded = jwt.verify(token, ENV.JWT_SECRET) as IUserModel;

		req.body.user = {
			userId: decoded._id,
			name: decoded.name,
			email: decoded.email,
			role: decoded.role,
		};

		next();
	} catch (err) {
		// err
		res.status(401).send(new ApiResponse(false, err.message));
	}
};
