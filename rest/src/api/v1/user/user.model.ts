import { Schema, model, Document } from 'mongoose';

import { IUser } from './user.interface';
import { EUserRoles, _hashText } from '../../../shared';

const userSchema = new Schema(
	{
		deletedAt: { type: Date },
		name: { type: String },
		role: { type: String, enum: [EUserRoles.admin, EUserRoles.user] },
		email: { type: String },
		password: { type: String, minlength: 8, maxlength: 24 },
	},
	{ timestamps: true }
);

userSchema.pre<IUserModel>('save', function (next) {
	console.log('pre save hook in place');

	if (!this.isModified('password')) {
		return next();
	}

	this.password = _hashText(this.password);
	next();
});

export interface IUserModel extends Document, IUser {}

export const UserModel = model<IUserModel>('user', userSchema);
