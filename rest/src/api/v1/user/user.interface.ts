import { EUserRoles } from '../../../shared';

export interface IUser {
	name: string;
	role: EUserRoles;
	email: string;
	password: string;
}
