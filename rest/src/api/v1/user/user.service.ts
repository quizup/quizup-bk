import { UserModel } from './user.model';
import { AppError } from '../../../shared';
import { IUser } from './user.interface';

/**
 * @description Get user
 * @param {string} email
 */
export const getUser = async (email: string) => {
	try {
		return await UserModel.findOne({ email }).lean();
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};

/**
 * @description Save user
 * @param  {IUser} user
 */
export const saveUser = async (user: IUser) => {
	try {
		return await UserModel.create(user);
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};
