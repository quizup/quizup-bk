export interface IQuiz {
	_id?: string;
	subject: string;
	questions: IQuestion[];
	deletedAt: Date;
}

export interface IQuestion {
	description: string;
	options: IOption[];
	correctOption: boolean;
}

export interface IOption {
	text: string;
}
