import { IQuiz } from './quiz.interface';
import { QuizModel } from './quiz.model';
import { AppError, IGeneric } from '../../../shared';

/**
 * @description Create quiz
 * @param  {IQuiz} quiz
 */
export const createQuiz = async (quiz: IQuiz) => {
	try {
		const _quiz = new QuizModel(quiz);
		return await _quiz.save();
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};

/**
 * @description Get a quiz
 * @param {IGeneric} filter
 */
export const getQuiz = async (filter: IGeneric) => {
	try {
		return await QuizModel.find({ deletedAt: { $exists: false }, ...filter });
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};

/**
 * @description Get all quizzes
 */
export const getAllQuizzes = async (project: IGeneric = {}) => {
	try {
		return await QuizModel.find({ deletedAt: { $exists: false } }, project);
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};

/**
 * @description Update quiz
 * @param  {string} quizId
 * @param  {IQuiz} quiz
 */
export const updateQuiz = async (quizId: string, quiz: IQuiz) => {
	try {
		return await QuizModel.updateOne({ _id: quizId }, { ...quiz, updatedAt: Date.now() });
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};

/**
 * @description Delete quiz
 * @param  {string} quizId
 */
export const deleteQuiz = async (quizId: string) => {
	try {
		const _quiz = await QuizModel.findOne({ _id: quizId, deletedAt: { $exists: true } });

		if (_quiz) {
			return 'Quiz already deleted';
		} else {
			return await QuizModel.updateOne({ _id: quizId }, { deletedAt: new Date() });
		}
	} catch (error) {
		throw new AppError('Validation', error, false);
	}
};
