import { Schema, model, Document } from 'mongoose';

import { IQuiz } from './quiz.interface';

const quizSchema = new Schema(
	{
		deletedAt: { type: Date },
		subject: {
			type: String,
			required: true,
			// unique: true,
			lowercase: true,
		},
		questions: {
			type: [
				{
					description: {
						type: String,
						required: true,
					},
					options: { type: [String], required: true },
					correctOption: { type: Number, required: true },
				},
			],
			required: true,
		},
	},
	{ timestamps: true }
);

interface IQuizModel extends Document, IQuiz {}

export const QuizModel = model<IQuizModel>('quiz', quizSchema);
