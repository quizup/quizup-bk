import { Request, Response } from 'express';

import * as QuizService from './quiz.service';
import { IQuiz } from './quiz.interface';
import { ApiResponse } from '../../../shared';

export const createQuiz = async (req: Request, res: Response) => {
	try {
		const payload: IQuiz = req.body;

		const _quiz = payload._id ? await QuizService.updateQuiz(payload._id, payload) : await QuizService.createQuiz(payload);

		res.send(new ApiResponse(true, 'Quiz posted successfully', _quiz));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not create quiz', undefined, error.message));
	}
};

export const deleteQuiz = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;

		const _quizzes = await QuizService.deleteQuiz(id);

		res.send(new ApiResponse(true, 'Quiz deleted successfully', _quizzes));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not delete quiz', undefined, error.message));
	}
};

export const getAllQuizzes = async (req: Request, res: Response) => {
	try {
		const _quizzes = await QuizService.getAllQuizzes();

		res.send(new ApiResponse(true, 'Quizzes fetched successfully', _quizzes));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get quiz', undefined, error.message));
	}
};

export const getQuizBySubject = async (req: Request, res: Response) => {
	try {
		const { subject } = req.params;

		const _quizzes = await QuizService.getQuiz({ subject: { $regex: new RegExp(`^${subject}$`, 'i') } });

		res.send(new ApiResponse(true, 'Quizzes fetched successfully', _quizzes));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get quiz', undefined, error.message));
	}
};

export const getQuizById = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;

		const _quiz = await QuizService.getQuiz({ _id: id });

		res.send(new ApiResponse(true, 'Quizzes fetched successfully', _quiz));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get quiz', undefined, error.message));
	}
};

export const updateQuiz = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;

		const updatedQuiz = req.body;

		const _quiz = await QuizService.updateQuiz(id, updatedQuiz);

		res.send(new ApiResponse(true, 'Quizzes fetched successfully', _quiz));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get quiz', undefined, error.message));
	}
};

export const getSubjects = async (req: Request, res: Response) => {
	try {
		const _subjects = await QuizService.getAllQuizzes({ subject: 1 });

		res.send(new ApiResponse(true, 'Subjects fetched successfully', _subjects));
	} catch (error) {
		res.send(new ApiResponse(false, 'Can not get subjects', undefined, error.message));
	}
};
