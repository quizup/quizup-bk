import { Router } from 'express';

import * as QuizController from './quiz.controller';
import { verifyToken, verifyRole } from '../user/middlewares';

const router = Router();

router
	.get('/', QuizController.getAllQuizzes)
	.post('/', verifyToken, verifyRole, QuizController.createQuiz)
	.get('/subjects', QuizController.getSubjects)
	.get('/subject/:subject', QuizController.getQuizBySubject)
	.get('/id/:id', QuizController.getQuizById)
	.delete('/:id', verifyToken, verifyRole, QuizController.deleteQuiz);

export const QuizRoutes = router;
