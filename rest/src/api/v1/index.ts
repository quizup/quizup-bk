import { Router } from 'express';

import { QuizRoutes } from './quiz/quiz.route';
import { UserRoutes } from './user/user.route';

// Init router and path
const V1Router = Router();

// Add sub-routes
V1Router.use('/quiz', QuizRoutes);
V1Router.use('/user', UserRoutes);

// Export the base-router
export default V1Router;
