import { Router } from 'express';
import V1 from './v1';

// Init router and path
const BaseRouter = Router();

// Add sub-routes
BaseRouter.use('/v1', V1);

// Export the base-router
export default BaseRouter;
