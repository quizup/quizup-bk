export enum EUserRoles {
	admin = 'admin',
	user = 'user',
}

export interface IGeneric {
	[key: string]: any;
}
