import bcrypt from 'bcrypt';

import { logger } from '../../config';

export class AppError extends Error {
	public readonly commonType: string;
	public readonly isOperational: boolean;

	constructor(commonType: string, description: any, isOperational: boolean) {
		super(description);

		Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain

		this.commonType = commonType;
		this.isOperational = isOperational;

		Error.captureStackTrace(this);

		// log errors for dev as well as prod environment
		logger.error(JSON.stringify({ commonType, description: description.message, isOperational }, undefined, 1));
	}
}

export class ApiResponse {
	public readonly success: boolean;
	public readonly message: string;
	public readonly data: any;
	public readonly error: any;

	constructor(success: boolean, message: string, data?: any, error?: string) {
		this.success = success;
		this.message = message;
		this.data = data;
		this.error = error;
	}
}

export const _compareHash = (plainText: string, hash: string) => bcrypt.compareSync(plainText, hash);

export const _hashText = (text: string) => bcrypt.hashSync(text, 10);
