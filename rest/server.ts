import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

import BaseRouter from './src/api';

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use('/api', BaseRouter);

app.get('/api/ping', (req, res) => res.send('pong'));

app.all('*', async (req, res) => res.status(404).send('Route not found'));

export { app };
