import mongoose from 'mongoose';

import { ENV } from './';
import { logger } from './';
import { AppError } from '../src/shared';

export const mongooseConn = async () => {
	try {
		const conn = await mongoose.connect(ENV.MONGO_URI, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			autoIndex: false,
		});
		if (conn) {
			// below log should be replaced with logger (logger should be written as a wrapper around logger/winston)
			logger.info('Connected to MongoDb');
		} else {
			logger.warn('Could not connect to mongodb');
		}
		return conn;
	} catch (err) {
		throw new AppError(err.type, 'Can not connect to MongoDB', true);
	}
};
