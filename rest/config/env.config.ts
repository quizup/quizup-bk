import { join } from 'path';
import dotenv from 'dotenv';

enum EEnvVars {
	NODE_ENV = 'NODE_ENV',
	PORT = 'PORT',
	HOST = 'HOST',
	JWT_SECRET = 'JWT_SECRET',
	JWT_EXPIRES_IN = 'JWT_EXPIRES_IN',
	MONGO_URI = 'MONGO_URI',
	API_VERSION = 'API_VERSION',
}

const getFromEnv = (name: EEnvVars) => process.env[name] as string;

const environment = process.env[EEnvVars.NODE_ENV];

const result = dotenv.config({
	path: join(__dirname, '..', 'env', `${environment}.env`),
});

if (result.error) throw result.error;

const ENV = {
	[EEnvVars.NODE_ENV]: getFromEnv(EEnvVars.NODE_ENV),
	[EEnvVars.PORT]: getFromEnv(EEnvVars.PORT),
	[EEnvVars.HOST]: getFromEnv(EEnvVars.HOST),
	[EEnvVars.JWT_SECRET]: getFromEnv(EEnvVars.JWT_SECRET),
	[EEnvVars.JWT_EXPIRES_IN]: getFromEnv(EEnvVars.JWT_EXPIRES_IN),
	[EEnvVars.MONGO_URI]: getFromEnv(EEnvVars.MONGO_URI),
	[EEnvVars.API_VERSION]: getFromEnv(EEnvVars.API_VERSION),
};

export { ENV };
